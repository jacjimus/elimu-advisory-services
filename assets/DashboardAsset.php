<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DashboardAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
   
    public $css = [
        
        '../themes/eac/assets/css/bootstrap.min.css',
        '../themes/eac/assets/font-awesome/4.2.0/css/font-awesome.min.css',
        '../themes/eac/assets/css/jquery-ui.custom.min.css',
        '../themes/eac/assets/css/jquery.gritter.min.css',
        '../themes/eac/assets/css/datepicker.min.css',
        '../themes/eac/assets/css/ace.min.css',
        
      
       
    ];
    public $js = [
        '../themes/eac/assets/js/jquery-ui.custom.min.js',
        '../themes/eac/assets/js/jquery.ui.touch-punch.min.js',
        '../themes/eac/assets/js/jquery.gritter.min.js',
        '../themes/eac/assets/js/bootbox.min.js',
        '../themes/eac/assets/js/bootstrap.min.js',
        '../themes/eac/assets/js/bootstrap-datepicker.min.js',
        '../themes/eac/assets/js/jquery.hotkeys.min.js',
        '../themes/eac/assets/js/bootstrap-wysiwyg.min.js',
        '../themes/eac/assets/js/select2.min.js',
        '../themes/eac/assets/js/fuelux.spinner.min.js',
        '../themes/eac/assets/js/jquery.maskedinput.min.js',
        '../themes/eac/assets/js/ace-elements.min.js',
        '../themes/eac/assets/js/ace.min.js',
        
         
    ];
    public $depends = [
       'yii\web\YiiAsset',
       'yii\bootstrap\BootstrapAsset',
       'justinvoelker\bootstrapnotifyalert\BootstrapNotifyAlertAsset',
    ];
}
