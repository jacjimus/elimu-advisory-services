<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
   
    public $css = [
        
        '../themes/eac/assets/font-awesome/4.2.0/css/font-awesome.min.css',
        '../themes/eac/assets/fonts/fonts.googleapis.com.css',
        '../themes/eac/css/style1.css',
      
       
    ];
    public $js = [
        '../themes/eac/js/jquery.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'justinvoelker\bootstrapnotifyalert\BootstrapNotifyAlertAsset',
    ];
}
