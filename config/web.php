<?php

$params = require(__DIR__ . '/params.php');
$config = parse_ini_file('secure.ini', true);
$config = [
    'id' => 'eas_portal',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'name' => 'Elimu Advisory Services (EAS)',
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => [
                        '@app/themes/eac',
                       ],
                     '@dektrium/user/views' => '@app/themes/eac/user'
                ],
                'baseUrl' => '@web/../themes/eac',
            ],
        ],
        'MyYiiUtils' => [
 
            'class' => 'app\components\MyYiiUtils',
 
            ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'clientId' => $config['oauth_facebook_key'],
                    'clientSecret' => $config['oauth_facebook_secret'],
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                    'clientId' => $config['google_client_id'],
                    'clientSecret' => $config['google_client_secret'],
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    'consumerKey' => $config['oauth_twitter_key'],
                    'consumerSecret' => $config['oauth_twitter_secret'],
                ],
                
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => $config['cookie_val_key'],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'admin/default/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => $config['smtp_host'],
                'username' => $config['smtp_username'],
                'password' => $config['smtp_password'],
                'port' => $config['smtp_port'],
                'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
    'modules' => ['user' => [
            'class' => 'dektrium\user\Module',
            'enableUnconfirmedLogin' => true,
            'confirmWithin' => 21600,
            'cost' => 12,
            'modelMap' => ["RegistrationForm" => 'app\models\RegistrationForm' , 'User' => 'app\models\User'],
            'admins' => ['admin'],
            'controllerMap' => [
                'registration' => [
                    'class' => \dektrium\user\controllers\RegistrationController::className(),
                    'on ' . \dektrium\user\controllers\RegistrationController::EVENT_AFTER_REGISTER => function ($e) {
                        Yii::$app->response->redirect(array('/user/security/login'))->send();
                        Yii::$app->end();
                    }
                        ],
                    ],
                    'mailer' => [
                        'sender' => 'no-reply@bremak.co.ke', // or ['no-reply@myhost.com' => 'Sender name']
                        'welcomeSubject' => 'Welcome to the Elimu Advisory Services Portal',
                        'confirmationSubject' => 'Thank you for Registering at Elimu Advisory Services Portal',
                        'reconfirmationSubject' => 'Email change subject',
                        'recoverySubject' => 'Recovery subject',
                    ],
                ],
                'admin' => [
                    'class' => 'app\modules\admin\Module',
                ]
            ],
        ];

        if (YII_ENV_DEV) {
            // configuration adjustments for 'dev' environment
            $config['bootstrap'][] = 'debug';
            $config['modules']['debug'] = [
                'class' => 'yii\debug\Module',
            ];

            $config['bootstrap'][] = 'gii';
            $config['modules']['gii'] = [
                'class' => 'yii\gii\Module',
            ];
        }

        return $config;
        