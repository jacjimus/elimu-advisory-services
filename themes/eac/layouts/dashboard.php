<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\DashboardAsset;
use justinvoelker\bootstrapnotifyalert\FlashAlert;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\base\view;

DashboardAsset::register($this);
if (isset($this->params['bootstrapNotifyAlertAssetInHead'])) {
    $this->registerAssetBundle(justinvoelker\bootstrapnotifyalert\BootstrapNotifyAlertAsset::className(), $this::POS_HEAD);
}
?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="index, follow">
        <?= Html::csrfMetaTags() ?>
        <title><?= isset($this->params['pageTitle']) ? $this->params['pageTitle'] : Yii::$app->name?></title>

        <?php $this->head() ?>
        <?= $this->render('_headers') ?>

    </head>
    <body class="navbar-fixed no-skin breadcrumbs-fixed">
        <?php $this->beginBody() ?>
        <?= $this->render('partials/_navbar_logged') ?>
        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>
                    <?= $this->render('partials/_sideNav') ?>
            <div class="main-content">
                <div class="main-content-inner">
                    <?= $this->render('partials/_breadcrumbs') ?>
                    <div class="page-content" style="max-height: 550px !important; overflow-x: hidden !important; overflow-y: scroll !important;">
                        <div class="page-header">
                            <h2 class="text-success">
                                <?= isset($this->params['pageTitle']) ? $this->params['pageTitle'] : Yii::$app->name?>
                            </h2>
                        </div><!-- /.page-header -->
                        <div class="row">
                            <?= $this->render('_alert') ?>
                            <?= $content ?>
                        </div> <!--end row-->
                    </div> <!--end page content-->
                </div> <!--end main content inner-->
                
            </div> <!--end main content-->
            <div class="footer">
                <div class="footer-inner">
                    <div class="footer-content">
                        <span class="bigger-120">
                            <span class="green bolder">&COPY;&nbsp;<?= Yii::$app->name ?></span>
                            <?= date('Y') ?>
                        </span>

                        &nbsp; &nbsp;
                        <span class="action-buttons">
                            <?= Html::a('<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>', 'https://www.facebook.com/elimuadvisory', ['target' => '_blank']) ?>
                            <?= Html::a('<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>', 'https://www.facebook.com/elimuadvisory', ['target' => '_blank']) ?>
                            <?= Html::a('<i class="ace-icon fa fa-rss-square orange bigger-150"></i>', 'https://www.facebook.com/elimuadvisory', ['target' => '_blank']) ?>
                        </span>
                    </div>
                </div>
            </div>  <!--end of footer div -->



            

        </div><!-- /.main-container -->
        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="<?= $this->theme->baseUrl ?>/assets/js/jquery.2.1.1.min.js"></script>

        <!-- <![endif]-->

        <!--[if IE]>
        <script src="assets/js/jquery.1.11.1.min.js"></script>
        <![endif]-->

        <!--[if !IE]> -->
        <script type="text/javascript">
                window.jQuery || document.write("<script src='<?= $this->theme->baseUrl ?>/assets/js/jquery.min.js'>" + "<" + "/script>");
        </script>

        <!-- <![endif]-->

        <!--[if IE]>
        <script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery1x.min.js'>"+"<"+"/script>");
        </script>
        <![endif]-->
        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='<?= $this->theme->baseUrl ?>/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
        </script>

        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='<?= $this->theme->baseUrl ?>/assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
            window.jQuery || document.write("<script src='<?= $this->theme->baseUrl ?>/assets/js/jquery.min.js'>" + "<" + "/script>");
        </script>
<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
