<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

?>
<header class="navbar navbar-default navbar-fixed-top navbar-top">
    <div class="navbar-container">
        <div class="navbar-header logo col-md-5">
            <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<?php echo Yii::$app->homeUrl ?>" class="navbar-brand "><?php echo Html::img($this->theme->baseUrl . "/img/logo.png", ["height" => 68, "width" => 90, "class" => ""]) ?></a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav" style="padding-top: 10px !important;">
                <li class="new-ads" ><a href="#" class="btn btn-ads btn-lg">Are you an Institution? </a></li>

            </ul>
                    
            <div class="navbar-buttons navbar-header pull-right" role="navigation" style="width: 200px !important;">
                  <ul class="nav ace-nav">
                       <?php if(Yii::$app->user->isGuest): ?>
                      <li class="light-green">
                          <?=Html::a("<div class='btn btn-sm btn-default'><i class=\"fa fa-user\"></i>&nbsp;Login</div>", \yii\helpers\Url::to(['/user/login'])); ?>
                      </li>
                   
                  </ul>
              </div>
            <?php endif;?>
                </li>

            </ul>
        </div>
    </div>
</header>