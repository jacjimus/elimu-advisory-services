<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\DashboardAsset;
use justinvoelker\bootstrapnotifyalert\FlashAlert;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\base\view;

DashboardAsset::register($this);
if (isset($this->params['bootstrapNotifyAlertAssetInHead'])) {
    $this->registerAssetBundle(justinvoelker\bootstrapnotifyalert\BootstrapNotifyAlertAsset::className(), $this::POS_HEAD);
}
?>
<?php $this->beginPage() ?>
<?php $this->title = 'Elimu Advisor'; ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="index, follow">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <?=$this->render('_headers')?>

    </head>
    <body>
        <?php $this->beginBody() ?>
         <div class="wrapper">
           <?=$this->render('_nav')?>
             <?php if(!Yii::$app->user->isGuest)$this->render('partials/_sideNav')?>
            <?php echo $content ?>
            <?=
            FlashAlert::widget([
                'showProgressbar' => false,
                'registerAsDefaults' => true,
                'registerPosition' => (isset($this->params['bootstrapNotifyAlertAssetInHead'])) ? $this::POS_BEGIN : null,
                'delay' => 10000,
                
            ])
            ?>      
            
        </div>
        
		<script type="text/javascript">
            $(document).ready(function () {
                // Code for highlighting selected Search categories
                $(".light").on("click", function () { // you can also use `on()` instead of `live()` which is deprecated
                    var id = this.id;
                    for (i = 1; i < 7; i++) {
                        if ($('#box' + i).hasClass('selected')) {
                            $('#box' + i).toggleClass("selected");
                        }
                    }

                    $("#box" + id).toggleClass('selected');
                    switch (id)
                    {
                        case '1':
                            search = "professions";
                            break;
                        case '2':
                            search = "undergrad";
                            break;
                        case '3':
                            search = "postgrad";
                            break;
                        case '4':
                            search = "tertiary";
                            break;
                        case '5':
                            search = "kmtc";
                            break;
                        default:
                            search = "others";
                            break;

                    }
                    $("#category").val(search);
                });


                // start-search
                $("#start-search").on("click", function () { // you can also use `on()` instead of `live()` which is deprecated

                    var searchme = $("#category").val();
                    
                    if (searchme === '')
                    {
                        $.notify("Warning: Please select a category to search from", "warn");
                        return false;
                    }
                           
                                      
                });




            });


        </script>
       
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
