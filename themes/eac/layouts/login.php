<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\LoginAsset;
use justinvoelker\bootstrapnotifyalert\FlashAlert;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\base\view;

LoginAsset::register($this);
if (isset($this->params['bootstrapNotifyAlertAssetInHead'])) {
    $this->registerAssetBundle(justinvoelker\bootstrapnotifyalert\BootstrapNotifyAlertAsset::className(), $this::POS_HEAD);
}
?>
<?php $this->beginPage() ?>
<?php $this->title = 'Elimu Advisor'; ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="index, follow">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <?= $this->render('_headers') ?>

    </head>
    <body class="login-layout light-login">
        <?php $this->beginBody() ?>
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">


                            <div class="space-6"></div>

                            <div class="position-relative">
                              
                                            <?= $content ?>
                                        
                            </div><!-- /.position-relative -->


                        </div>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.main-content -->
        </div>
        <!--[if !IE]> -->
        <script type="text/javascript">
            if ('ontouchstart' in document.documentElement)
                document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
            window.jQuery || document.write("<script src='assets/js/jquery.min.js'>" + "<" + "/script>");
        </script>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
