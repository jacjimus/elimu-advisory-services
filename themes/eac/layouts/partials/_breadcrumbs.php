<?php
use yii\widgets\Breadcrumbs;
?>
<div class="breadcrumbs breadcrumbs-fixed" id="breadcrumbs">
    <i class='ace-icon blue fa fa-home' style="margin-right: -10px; margin-left: 10px;"></i><?= 
   Breadcrumbs::widget([
    'homeLink' => [
    'label' => Yii::t('yii', "Home"),
    'url' => Yii::$app->urlManager->createUrl('user/profile/index'),
    ],
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
   ]) ;
?>
       
</div>
