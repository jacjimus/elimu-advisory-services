<?php
use yii\helpers\Html;
?>
<div class="navbar navbar-default navbar-fixed-top" id="navbar">
    <script type="text/javascript">
        try {
            ace.settings.check('navbar', 'fixed')
        } catch (e) {
        }
    </script>


    <div class="navbar-container" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
            <a href="<?php echo Yii::$app->homeUrl ?>"><?php echo Html::img($this->theme->baseUrl . "/img/logo_white.png", ["height" => 45, "width" => 60, "class" => ""]) ?></a>

        </div>

        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <ul class="nav ace-nav">


                <li class="light-green">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <img class="nav-user-photo" src="<?= $this->theme->baseUrl ?>/assets/avatars/user.png" alt="Jason's Photo" />
                        <span class="user-info">
                            <small>Welcome,</small>
                            <?= Yii::$app->user->identity->username; ?>
                        </span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('user/settings/account')?>">
                                <i class="ace-icon fa fa-lock"></i>
                                Change password
                            </a>
                        </li>

                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('user/profile/account')?>">
                                <i class="ace-icon fa fa-user"></i>
                                My Profile
                            </a>
                        </li>
                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('user/settings/networks')?>">
                                <i class="ace-icon fa fa-facebook"></i>
                                Social Accounts
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="<?=Yii::$app->urlManager->createUrl('user/logout')?>">
                                <i class="ace-icon fa fa-power-off"></i>
                                Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div><!-- /.navbar-container -->
</div>