<div id="sidebar" class="sidebar responsive">
    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'fixed')
        } catch (e) {
        }
    </script>
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="ace-icon fa fa-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="ace-icon fa fa-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </button>

            <button class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li class="<?= in_array(Yii::$app->controller->action->id , ['show' , 'account', 'networks']) ? 'active open' : ''?>">
            <a href="#" class="dropdown-toggle open">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>
            <ul class="submenu">
                <li class="<?=Yii::$app->controller->action->id === 'account' ? 'active open' : ''?>">
                    <a href="<?=Yii::$app->urlManager->createUrl('user/profile/account')?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        My Profile
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?=Yii::$app->controller->action->id === 'search' ? 'active open' : ''?>">
                    <a href="#">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Search History
                    </a>

                    <b class="arrow"></b>
                </li>

                <li class="<?=Yii::$app->controller->action->id === 'networks' ? 'active open' : ''?>">
                    <a href="<?=Yii::$app->urlManager->createUrl('user/settings/networks')?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Social Accounts
                    </a>

                    <b class="arrow"></b>
                </li>

                
            </ul>
        </li>


        <li class="<?=Yii::$app->controller->id === 'billing' ? 'active open' : ''?>">
            <a href="">
                <i class="menu-icon fa fa-list-alt"></i>
                <span class="menu-text"> Billing History </span>
            </a>

            <b class="arrow"></b>
            
        </li>

        <li class="<?=Yii::$app->controller->action->id === 'search' ? 'active open' : ''?>">
            <a href="<?=Yii::$app->urlManager->createUrl('admin/default/search')?>">
                <i class="menu-icon fa fa-search"></i>

                <span class="menu-text">
                    Search

                    <span class="badge badge-transparent tooltip-error" title="Career Search">
                        <i class="ace-icon fa fa-exclamation-triangle red bigger-130"></i>
                    </span>
                </span>
            </a>

            <b class="arrow"></b>
        </li>

        <li class="<?=Yii::$app->controller->id === 'alerts' ? 'active open' : ''?>">
            <a href="">
                <i class="menu-icon fa fa-exclamation-circle"></i>
                <span class="menu-text">Manage Alerts </span>
            </a>

            <b class="arrow"></b>
        </li>




    </ul><!-- /.nav-list -->


    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>

    <script type="text/javascript">
        try {
            ace.settings.check('sidebar', 'collapsed')
        } catch (e) {
        }
    </script>
</div>




