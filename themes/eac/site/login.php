<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="main-content ">
    <div class="row">
        <div class="col-sm-10 col-md-4 col-sm-offset-1 col-md-offset-3">
            <div class="login-container">

                <div class="space-6"></div>
                <div class="position-relative">
                    <div id="login-box" class="login-box visible widget-box no-border">
                        <div class="widget-body">
                            <div class="widget-main">
                                <h4 class="header blue lighter bigger">
                                    <i class="ace-icon fa fa-user-md green"></i>
                                    Please enter your login Information
                                </h4>

                                <div class="space-6"></div>
                                <?php
                                $form = ActiveForm::begin([
                                            'id' => 'login-form',
                                            'class' => 'form-horizontal',
                                ]);
                                ?>
                                <fieldset>
                                     <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Email address', 'class' => 'form-control']) ?>
                                            

                                   <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password', 'class' => 'form-control']) ?>
                                            

                                    <div class="space"></div>

                                    <div class="clearfix">
                                        <label class="inline">
                                            <?=
                                            $form->field($model, 'rememberMe')->checkbox([
                                                'template' => "<div class=\"col-md-offset-1 col-md-10\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
                                            ])
                                            ?>
                                            
                                        </label>
                                        <?= Html::submitButton(' <i class="ace-icon fa fa-key"></i><span class="bigger-110">Login</span>', ['class' => 'width-35 pull-right btn btn-sm btn-primary', 'name' => 'login-button']) ?>

                                    </div>

                                    <div class="space-4"></div>
                                </fieldset>
                                <?php ActiveForm::end(); ?>

                                <div class="social-or-login center">
                                    <span class="bigger-110">Or Login Using</span>
                                </div>

                                <div class="social-login center">
                                    <a class="btn btn-primary">
                                        <i class="ace-icon fa fa-facebook"></i>
                                    </a>

                                    <a class="btn btn-info">
                                        <i class="ace-icon fa fa-twitter"></i>
                                    </a>

                                    <a class="btn btn-danger">
                                        <i class="ace-icon fa fa-google-plus"></i>
                                    </a>
                                </div>
                            </div><!-- /.widget-main -->

                            <div class="footer col-md-12">
                                <div class="col-md-6">
                                    <a href="#" data-target="#forgot-box" class="forgot-password-link">
                                        <i class="ace-icon fa fa-arrow-left"></i>
                                        I forgot my password
                                    </a>
                                </div>

                                <div class="col-md-6">
                                    <a href="#" data-target="#signup-box" class="user-signup-link">
                                        I want to register
                                        <i class="ace-icon fa fa-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div><!-- /.widget-body -->
                    </div><!-- /.login-box -->

                </div><!-- /.position-relative -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.main-content -->
</div><!-- /.main-container -->


</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
   jQuery(function($) {
			 $(document).on('click', '.toolbar a[data-target]', function(e) {
				e.preventDefault();
				var target = $(this).data('target');
				$('.widget-box.visible').removeClass('visible');//hide others
				$(target).addClass('visible');//show target
			 });
			});
</script>
