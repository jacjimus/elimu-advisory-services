<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\AppAsset;
AppAsset::register($this);
$this->params['bootstrapNotifyAlertAssetInHead'] = true;

?>

<section class="hero">
    <div class="bg"></div>
    <div class="container text-center">
        <div class="row center-block" style="margin-bottom: 10px;">
            
            <h2 class="hero-title ">Build your career with us</h2>
            <p class="hero-description hidden-xs">Find all Universities & colleges in Kenya with their Programmes offered here. We give you a simple way.</p>

        </div>
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="row">
                    <a href="javascript:void(0)" class="light" id="1">
                        <div class="col-xs-12 col-sm-4">
                            <div id="box1" class="shortcut">
                                <i class="fa fa-institution shortcut-icon icon-blue"></i>
                                <h3>Professions</h3>
                                <span class="total-items">234,567</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" class="light" id="2">
                        <div class="col-xs-12 col-sm-4">
                            <div id="box2" class="shortcut">
                                <i class="fa fa-graduation-cap shortcut-icon icon-green"></i>
                                <h3>Under-Graduate</h3>
                                <span class="total-items">25,366</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" class="light" id="3">
                        <div class="col-xs-12 col-sm-4">
                            <div id="box3" class="shortcut">
                                <i class="fa fa-institution shortcut-icon icon-brown"></i>
                                <h3>Post-Graduate</h3>
                                <span class="total-items">252,546</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" class="light" id="4">
                        <div  class="col-xs-12 col-sm-4">
                            <div id="box4" class="shortcut">
                                <i class="fa fa-building-o shortcut-icon icon-violet"></i>
                                <h3>Tertiary Colleges</h3>
                                <span class="total-items">52,546</span>
                            </div>
                        </div>
                    </a>
                    <a href="javascript:void(0)" class="light" id="5">
                        <div class="col-xs-12 col-sm-4">
                            <div id="box5" class="shortcut">
                                <i class="fa fa-hospital-o shortcut-icon icon-dark-blue"></i>
                                <h3>KMTC - Colleges</h3>
                                <span class="total-items">215,546</span>
                            </div>
                        </div>
                    </a>

                    <a href="javascript:void(0)" class="light" id="6">
                        <div class="col-xs-12 col-sm-4">
                            <div id="box6" class="shortcut">
                                <i class="fa fa-university shortcut-icon icon-light-blue"></i>
                                <h3>Other Colleges</h3>
                                <span class="total-items">15,546</span>
                            </div>  
                        </div>
                    </a>

                </div>

            </div>
            <div class="col-md-4 col-sm-12">
                <div class="widget">
                    <div class="widget-header-green">
                        <h3 class="white-opaque">Become a member</h3>
                    </div>
                    <div class="widget-body">
                        

                        <?=
                        dektrium\user\widgets\Connect::widget([
                            'baseAuthUrl' => ['/user/security/auth'],
                        ])
                        ?>
                        <span class="text-primary">--------- OR --------</span>
                        <div class="row">
                        <div class="col-md-6 col-xs-10">
                            <?= Html::a('<div class="btn btn-sm btn-default col-md-12 col-xs-12"><i class="fa fa-lock"></i>Login</div>', yii\helpers\Url::to("user/login"), ["class" => "col-md-12 col-xs-12"]) ?>
                        </div>
                        <div class="col-md-6 col-xs-10">
                            <?= Html::a('<div class="btn btn-sm btn-default col-md-12 col-xs-12"><i class="fa fa-user"></i>Register</div>', yii\helpers\Url::to("user/register"), ["class" => "col-md-12 col-xs-12"]) ?>
                        </div>
                        </div>
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="col-md-offset-2 col-md-6">
<?php
$form = ActiveForm::begin([
            'id' => 'search-form',
            'enableAjaxValidation' => true,
            'validateOnSubmit' => true,
            'action' => ['site/wizard'],
        ])
?>
                    <?= Html::hiddenInput('category', '', ['id' => 'category']) ?>

                    <div class="row text-center">
                        <div class="col-md-2"></div>
                        <div class="col-md-4 col-sm-6">
                            <?php echo Html::submitButton("<i class=\"fa fa-search\"></i>&nbsp;Search", ["id" => "start-search", "class" => "btn btn-white btn-block btn-lg col-md-10 col-xs-10", "style" => "background: #406619 !important; color: #fff !important;", "onclick" => "javascript:void(0)"]) ?>

                        </div>
                        <div class="col-md-4"></div>
                    </div>
                    <?php ActiveForm::end(); ?>   
                </div>

                <div class="col-md-4" id="list3">
                    <ul>
                        <li>Are you a recent KCSE graduate?</li>
                        <li>Are you an institution?</li>
                        <li>Do you have difficulty in selecting your career?</li>
                        <li>Select one category above and search opportunities</li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="counter">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="item-counter">
                    <span class="item-icon"><i class="fa fa-search-plus"></i></span>
                    <div data-refresh-interval="100" data-speed="3000" data-to="7803" data-from="0" class="item-count">7803</div>
                    <span class="item-info">Searches</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item-counter">
                    <span class="item-icon"><i class="fa fa-group"></i></span>
                    <div data-refresh-interval="50" data-speed="5000" data-to="427" data-from="0" class="item-count">427</div>
                    <span class="item-info">Members</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item-counter">
                    <span class="item-icon"><i class="fa fa-building-o"></i></span>
                    <div data-refresh-interval="80" data-speed="5000" data-to="639" data-from="0" class="item-count">639</div>
                    <span class="item-info">Institutions</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="item-counter">
                    <span class="item-icon"><i class="fa fa-calendar-check-o"></i></span>
                    <div data-refresh-interval="80" data-speed="5000" data-to="1548" data-from="0" class="item-count">1548</div>
                    <span class="item-info">Programmes</span>
                </div>
            </div>
        </div>
    </div> <!-- / .counter -->
</div>
</section>




