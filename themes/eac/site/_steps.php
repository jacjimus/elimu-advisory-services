
<ul class="steps">
    <li data-step="1" class="<?=$step == 1  ? 'active ' : '' ?><?=$step > 1  ? ' complete' : '' ?> " >
        <span class="step">1</span>
        <span class="title">Define your search</span>
    </li>

    <li data-step="2" class="<?=$step == 2 ? 'active' : '' ?><?=$step > 2  ? ' complete' : '' ?> ">
        <span class="step">2</span>
        <span class="title">Enter Qualifications</span>
    </li>

    <li data-step="3" class="<?=$step == 3 ? 'active' : '' ?><?=$step > 3  ? ' complete' : '' ?> ">
        <span class="step">3</span>
        <span class="title">Preferred Location</span>
    </li>

    <li data-step="4" class="<?=$step == 4 ? 'active' : '' ?><?=$step > 4  ? ' complete' : '' ?> ">
        <span class="step">4</span>
        <span class="title">Other Info</span>
    </li>
    <li data-step="5" class="<?=$step == 5 ? 'active' : '' ?><?=$step > 5  ? ' complete' : '' ?> ">
        <span class="step">5</span>
        <span class="title">Summary Reports</span>
    </li>
</ul>
