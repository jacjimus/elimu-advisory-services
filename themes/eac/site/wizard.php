<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\base\View;
$user = Yii::$app->user->identity;
$this->title = 'Search';
$this->params['breadcrumbs'][] = ['label' => "Dashboard", 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = Yii::t('user', 'Search wizard');

?>

<div class="main-content ">
    <div class="row">
    <div class=" col-md-offset-1 col-md-10">
        <!-- PAGE CONTENT BEGINS -->
        
        <div class="widget-box">
            <div class="widget-header widget-header-green widget-header-flat">
                <h3 class="widget-title lighter"
                    <i class="ace-icon fa fa-hand-o-right icon-animated-hand-pointer green"></i>
                    <a href="#modal-wizard" data-toggle="modal" class="green"> Career / Courses search wizard Form </a>
                </h3>
                <div class="widget-toolbar">
                    <label>
                        <small class="green">
                            <b>Validation</b>
                        </small>

                        <input id="skip-validation" type="checkbox" class="ace ace-switch ace-switch-4" />
                        <span class="lbl middle"></span>
                    </label>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main">
                    <div id="fuelux-wizard-container">
                        <div>
                            <?= $this->render("_steps")?>
                        </div>

                        <hr />

                        <div class="step-content pos-rel">
                            <?=$this->render("steps/_step1", ['user_search_model' =>  $user_search_model])?> 

                            <div class="step-pane" data-step="2">
                                <div>
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="ace-icon fa fa-times"></i>
                                        </button>

                                        <strong>
                                            <i class="ace-icon fa fa-check"></i>
                                            Well done!
                                        </strong>

                                        You successfully read this important alert message.
                                        <br />
                                    </div>

                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="ace-icon fa fa-times"></i>
                                        </button>

                                        <strong>
                                            <i class="ace-icon fa fa-times"></i>
                                            Oh snap!
                                        </strong>

                                        Change a few things up and try submitting again.
                                        <br />
                                    </div>

                                    <div class="alert alert-warning">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="ace-icon fa fa-times"></i>
                                        </button>
                                        <strong>Warning!</strong>

                                        Best check yo self, you're not looking too good.
                                        <br />
                                    </div>

                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="ace-icon fa fa-times"></i>
                                        </button>
                                        <strong>Heads up!</strong>

                                        This alert needs your attention, but it's not super important.
                                        <br />
                                    </div>
                                </div>
                            </div>

                            <div class="step-pane" data-step="3">
                                <div class="center">
                                    <h3 class="blue lighter">This is step 3</h3>
                                </div>
                            </div>

                            <div class="step-pane" data-step="4">
                                <div class="center">
                                    <h3 class="green">Congrats!</h3>
                                    Your product is ready to ship! Click finish to continue!
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr />
                    <div class="wizard-actions">
                        <button class="btn btn-prev">
                            <i class="ace-icon fa fa-arrow-left"></i>
                            Prev
                        </button>

                        <button class="btn btn-success btn-next" data-last="Finish">
                            Next
                            <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                        </button>
                    </div>
                </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
        </div>

        <div id="modal-wizard" class="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div id="modal-wizard-container">
                        <div class="modal-header">
                            <ul class="steps">
                                <li data-step="1" class="active">
                                    <span class="step">1</span>
                                    <span class="title">Validation states</span>
                                </li>

                                <li data-step="2">
                                    <span class="step">2</span>
                                    <span class="title">Alerts</span>
                                </li>

                                <li data-step="3">
                                    <span class="step">3</span>
                                    <span class="title">Payment Info</span>
                                </li>

                                <li data-step="4">
                                    <span class="step">4</span>
                                    <span class="title">Other Info</span>
                                </li>
                            </ul>
                        </div>

                        <div class="modal-body step-content">
                            <div class="step-pane active" data-step="1">
                                <div class="center">
                                    <h4 class="blue">Step 1</h4>
                                </div>
                            </div>

                            <div class="step-pane" data-step="2">
                                <div class="center">
                                    <h4 class="blue">Step 2</h4>
                                </div>
                            </div>

                            <div class="step-pane" data-step="3">
                                <div class="center">
                                    <h4 class="blue">Step 3</h4>
                                </div>
                            </div>

                            <div class="step-pane" data-step="4">
                                <div class="center">
                                    <h4 class="blue">Step 4</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer wizard-actions">
                        <button class="btn btn-sm btn-prev">
                            <i class="ace-icon fa fa-arrow-left"></i>
                            Prev
                        </button>

                        <button class="btn btn-success btn-sm btn-next" data-last="Finish">
                            Next
                            <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                        </button>

                        <button class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
                            <i class="ace-icon fa fa-times"></i>
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div><!-- PAGE CONTENT ENDS -->
    </div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.main-container -->


<script type="text/javascript">
			jQuery(function($) {
			
				$('[data-rel=tooltip]').tooltip();
			
				$(".select2").css('width','200px').select2({allowClear:true})
				.on('change', function(){
					$(this).closest('form').validate().element($(this));
				}); 
			
			
				var $validation = false;
				$('#fuelux-wizard-container')
				.ace_wizard({
					//step: 2 //optional argument. wizard will jump to step "2" at first
					//buttons: '.wizard-actions:eq(0)'
				})
				.on('actionclicked.fu.wizard' , function(e, info){
					if(info.step == 1 && $validation) {
						if(!$('#validation-form').valid()) e.preventDefault();
					}
				})
				.on('finished.fu.wizard', function(e) {
					bootbox.dialog({
						message: "Thank you! Your information was successfully saved!", 
						buttons: {
							"success" : {
								"label" : "OK",
								"className" : "btn-sm btn-primary"
							}
						}
					});
				}).on('stepclick.fu.wizard', function(e){
					//e.preventDefault();//this will prevent clicking and selecting steps
				});
			
			
			
				$.mask.definitions['~']='[+-]';
				$('#phone').mask('(999) 999-9999');
			
				jQuery.validator.addMethod("phone", function (value, element) {
					return this.optional(element) || /^\(\d{3}\) \d{3}\-\d{4}( x\d{1,6})?$/.test(value);
				}, "Enter a valid phone number.");
			
				
				$('#modal-wizard-container').ace_wizard();
				$('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
				
				
				/**
				$('#date').datepicker({autoclose:true}).on('changeDate', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
				
				$('#mychosen').chosen().on('change', function(ev) {
					$(this).closest('form').validate().element($(this));
				});
				*/
				
				
				$(document).one('ajaxloadstart.page', function(e) {
					//in ajax mode, remove remaining elements before leaving page
					$('[class*=select2]').remove();
				});
			})
		</script>
<?php $this->registerCssFile($this->theme->baseUrl . "/assets/css/select2.min.css");
$this->registerCssFile($this->theme->baseUrl . "/css/steps.css");
