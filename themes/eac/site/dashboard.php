<?php
use yii\helpers\Html;
use yii\helpers\Url;
use dektrium\user\models\User;
$user_model = Yii::$app->user->identity;
//echo $this->renderPartial('_profile_tabs', array('model' => $user_model)); ?>
<div id="user-profile-1" class="user-profile row">
        <div class="col-xs-12 col-sm-3 center">
                <div>
                        <span class="profile-picture">
                            <div class="fileUpload btn btn-warning">
                                <span>Update Profile</span>
                                <input type="file" accept="image/*" onchange="loadFile(event)" class="upload"/>
                            </div></span>
                    
                    
                        <div class="space-4"></div>
                        <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
                                <div class="inline position-relative">
                                        <a href="javascript:void(0);" class="user-title-label dropdown-toggle" data-toggle="dropdown">
                                                <i class="icon-circle light-green yellow  middle"></i>
                                                &nbsp;
                                                <span class="white"><?php echo Html::encode($user_model->username) ?></span>
                                        </a>
                                </div>
                        </div>
                </div>
                <div class="space-6"></div>
                 <div class="profile-contact-info">
                        <div class="profile-contact-links align-left">
                                <ul class="list-unstyled">
                                   
                                    <li><?php echo Html::a( "<i class='fa fa-asterisk bigger-120 green'></i>Change your password" ,'user/settings/profile', ['class' => 'btn btn-link']) ?></a></li>
                                    <li><?php echo Html::a( "<i class='fa fa-asterisk bigger-120 green'></i>Account details" ,'user/settings/account', ['class' => 'btn btn-link']) ?></a></li>
                                    <li><?php echo Html::a( "<i class='fa fa-asterisk bigger-120 green'></i>Connect Social accounts" ,'user/settings/networks', ['class' => 'btn btn-link']) ?></a></li>

                                </ul>
                        </div>
                </div>
                
        </div>
        <div class="col-xs-12 col-sm-9">
                <div class="panel-group" id="accordion">
                <?php $this->render('_view_account', ['model' => $user_model]) ?>
                                
                        </div>
                <?php //$this->render('_update_personal', ['model' => $person_model]) ?>
                <?php //$this->render('_update_account', ['model' => $user_model]) ?>
                <?php //$this->render('_update_address', ['model' => $address_model]) ?>
                <?php //$this->render('_reset_password', ['model' => $user_model]) ?>
                <?php //$this->render('_change_password', ['model' => $user_model]) ?>
                
        </div>
</div>
<script>
    var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
<?php //Yii::app()->clientScript->registerScript('users_' . $user_model->id, "MyController.Users.User.init();"); ?>