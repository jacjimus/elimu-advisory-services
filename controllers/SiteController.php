<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Search;
use app\models\ContactForm;
use app\components\MyYiiUtils;
use app\components\MyController;
use app\modules\admin\models\UserResult;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','search' , 'wizard'],
                'rules' => [
                    [
                        'actions' => ['logout' , 'search'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['wizard'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions() {
  return [
    'auth' => [
      'class' => 'yii\authclient\AuthAction',
      'successCallback' => [$this, 'oAuthSuccess'],
    ],
  ];
}

public function oAuthSuccess($client) {
  // get user data from client
  $userAttributes = $client->getUserAttributes();

  // do some thing with user data. for example with $userAttributes['email']
}
    public function actionIndex()
    {
        if(!\Yii::$app->user->isGuest)
            $this->redirect("user/profile");
        $model = new Search();
         if ($model->load(Yii::$app->request->post())) {
           
            return $this->redirect('wizard' , ['search' => $category]);
        }
         if(!Yii::$app->user->isGuest)
                return $this->render('dashboard');
         return $this->render('index');
    }

    public function actionWizard()
    {
        
        return $this->render('wizard', [
            'user_search_model' => new UserResult
        ]);
    }
    
    
    protected function getTitle($cat)
    {
        switch($cat)
        {
            case "professions":
                return "Professional Programmes";
                break;
            case "undergrad":
                return "Under graduate Programmes";
                break;
            case "postgrad":
                return "Post graduate Programmes";
                break;
            case "tertiary":
                return "Tertiary Programmes";
                break;
            case "kmtc":
                return "KMTC Medical Programmes";
                break;
           
            default :
                return "Other Categories";
                break;
        }
    }
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionDashboard()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
