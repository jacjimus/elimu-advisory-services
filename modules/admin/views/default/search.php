<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\ProgrammeTypes;
use app\modules\admin\models\EducationSystem;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;

use yii\helpers\Url;
use yii\base\View;

$user = Yii::$app->user->identity;
$this->title = Yii::t('user', 'Search wizard');
$this->params['breadcrumbs'][] = ['label' => "Dashboard", 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['search']];
$this->params['breadcrumbs'][] = Yii::t('user', 'Step 1');
$this->params['pageTitle'] = Yii::t('user', 'Search wizard');
?>

<div class="main-content ">
    <div class="row">
        <div class=" col-md-offset-1 col-md-10">
            <!-- PAGE CONTENT BEGINS -->

            <div class="widget-box">
                <div class="widget-header widget-header-green widget-header-flat">
                    <h3 class="widget-title lighter"
                        <i class="ace-icon fa fa-hand-o-right icon-animated-hand-pointer green"></i>
                        <a href="#" class="green"> Career / Courses search wizard Form </a>
                    </h3>
                    <div class="widget-toolbar">
                        <label>
                            <small class="green">
                                <b>Validation</b>
                            </small>

                            <input id="skip-validation" type="checkbox" checked="true" class="ace ace-switch ace-switch-4" />
                            <span class="lbl middle"></span>
                        </label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div id="fuelux-wizard-container">
                            <div>
                                <?= $this->render("@app/themes/eac/site/_steps", ['step' => $step]) ?>
                            </div>


                            <hr />

                            <div class="step-content pos-rel">
                                <div class="step-pane active" data-step="1">
                                    <?php
                                    $form = ActiveForm::begin([
                                                'id' => 'search-form',
                                                'enableAjaxValidation' => true,
                                                'validateOnSubmit' => false,
                                                "class" => "form-horizontal"
                                            ])
                                    ?>
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-5">
                                            <span class="block input-icon input-icon-right">
                                                <?php $catlist = [1 => 'Professional Career', 2 => 'Undergraduate Programmes', 3 => 'Postgraduate Programmes', 4 => 'Tertiary Institutions', 5 => 'KMTC Medical Courses', 6 => 'Other Colleges'] ?>
                                                <?= $form->field($user_search_model, 'education_cat_id')->dropDownList($catlist, ['prompt' => 'Select ', 'id' => 'education_cat_id']); ?>

                                                <i class="ace-icon fa fa-leaf"></i>
                                            </span>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12 col-sm-5">
                                            <span class="block input-icon input-icon-right">
                                                <?php
                                                // Child # 1
                                                echo $form->field($user_search_model, 'education_system')->widget(DepDrop::classname(), [
                                                    'options' => ['prompt' => 'Select ', 'id' => 'education_system_id'],
                                                    'pluginOptions' => [
                                                        'depends' => ['education_cat_id'], // the id for cat attribute
                                                        'placeholder' => 'Select ...',
                                                        'url' => Url::to(['default/subcat'])
                                                    ]
                                                ]);
                                                ?>

                                                <i class="ace-icon fa fa-wifi"></i>
                                            </span>
                                            
                                        </div>
                                    </div>





                                </div>

                            </div>
                        </div>

                        <div class="modal-footer wizard-actions">

                            <?= Html::submitButton("Next <i class=\"ace-icon fa fa-arrow-right icon-on-right\"></i>", ["class" => "btn btn-success btn-sm btn-next"]) ?>
                            <?= Html::a("Cancel<i class=\"ace-icon fa fa-times\"></i>", Yii::$app->homeUrl, ["class" => "btn btn-danger btn-sm pull-left"]) ?>


                        </div>
                        <?php ActiveForm::end(); ?> 
                    </div>
                </div>
            </div><!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.main-container -->

<?php
$this->registerCssFile($this->theme->baseUrl . "/assets/css/select2.min.css");
$this->registerCssFile($this->theme->baseUrl . "/css/steps.css");
$this->registerCssFile($this->theme->baseUrl . "/assets/css/chosen.min.css");
$this->registerJsFile($this->theme->baseUrl . "/assets/js/chosen.jquery.min.js");
$this->registerJsFile($this->theme->baseUrl . "/assets/js/select2.min.js");
