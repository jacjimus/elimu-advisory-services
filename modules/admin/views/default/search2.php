<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\ProgrammeTypes;
use app\modules\admin\models\EducationSystem;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\base\View;
use app\modules\admin\models\Groups;
use kartik\editable\Editable;
use kartik\select2\Select2;
use kartik\popover\PopoverX;
$user = Yii::$app->user->identity;
$this->title = Yii::t('user', 'Search');
$this->params['breadcrumbs'][] = ['label' => "Dashboard", 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['search']];
$this->params['breadcrumbs'][] = Yii::t('user', 'Step 2');
$this->params['pageTitle'] = Yii::t('user', 'Search wizard');
?>

<div class="main-content ">
    <div class="row">
        <div class=" col-md-offset-1 col-md-10">
            <!-- PAGE CONTENT BEGINS -->

            <div class="widget-box">
                <div class="widget-header widget-header-green widget-header-flat">
                    <h3 class="widget-title lighter"
                        <i class="ace-icon fa fa-hand-o-right icon-animated-hand-pointer green"></i>
                        <a href="#" class="green"> Career / Courses search wizard Form </a>
                    </h3>
                    <div class="widget-toolbar">
                        <label>
                            <small class="green">
                                <b>Validation</b>
                            </small>

                            <input id="skip-validation" type="checkbox" checked="true" class="ace ace-switch ace-switch-4" />
                            <span class="lbl middle"></span>
                        </label>
                    </div>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div id="fuelux-wizard-container">
                            <div>
                                <?= $this->render("@app/themes/eac/site/_steps", ['step' => $step]) ?>
                            </div>


                            <hr />

                            <div class="step-content pos-rel">
                                <div class="step-pane active" data-step="1">
                               
                                 <?php foreach (Groups::find()->all() As $group): ?>
                                    <div class="col-xs-12 col-sm-4 widget-container-col">
                                            <div class="widget-box">
                                                <div class="widget-header">
                                                    <h5 class="widget-title smaller"><?=$group->group_name?></h5>

                                                    <div class="widget-toolbar">
                                                        <span class="label label-success">
                                                            Max
                                                            <?=$group->max_subject?>
                                                            
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="widget-body">
                                                    <div class="widget-main padding-6">
                                                        <div class="alert alert-info"> 
                                                        <?php
                                                        $model = new \app\modules\admin\models\UserResult;
                                                            $editable = Editable::begin([
                                                                        'model' => $model,
                                                                        'attribute' => 'subject_code',
                                                                        'asPopover' => true,
                                                                        'size' => PopoverX::SIZE_MEDIUM,
                                                                        'inputType' => Editable::INPUT_DEPDROP,
                                                                        'options' => [
                                                                            'type' => DepDrop::TYPE_SELECT2,
                                                                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                                                            'options' => ['id' => "cat-".$group->group_no, 'placeholder' => 'Select grade...'],
                                                                            'pluginOptions' => [
                                                                                'depends' => [$group->group_no],
                                                                                'url' => Url::to(['/site/subcat'])
                                                                            ]
                                                                        ]
                                                            ]);
                                                            $form = $editable->getForm();
// use a hidden input to understand if form is submitted via POST
                                                            $editable->beforeInput = Html::hiddenInput('kv-editable-depdrop', 1) .
                                                                    $form->field($model, 'subject_code')->dropDownList(['' => 'Select subject...'] + app\modules\admin\models\SubjectGroup::findAll("group_no=$group->group_no"), ['id' => $group->group_no])->label(false) . "\n";
                                                                    
                                                            Editable::end();
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                   <?php endforeach;?>
                                </div>
                            </div>





                        </div>

                    </div>
                </div>

                <div class="modal-footer wizard-actions">
                    <button class="btn btn-sm btn-prev">
                        <i class="ace-icon fa fa-arrow-left"></i>
                        Prev
                    </button>

                    <button type="submit" class="btn btn-success btn-sm btn-next" data-last="Finish">
                        Next
                        <i class="ace-icon fa fa-arrow-right icon-on-right"></i>
                    </button>

                    <button class="btn btn-danger btn-sm pull-left" data-dismiss="modal">
                        <i class="ace-icon fa fa-times"></i>
                        Cancel
                    </button>
                </div>

            </div>
        </div>
    </div><!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.main-container -->

<?php
$this->registerCssFile($this->theme->baseUrl . "/assets/css/select2.min.css");
$this->registerCssFile($this->theme->baseUrl . "/css/steps.css");
$this->registerJsFile($this->theme->baseUrl . "/assets/js/fuelux.wizard.min.js");
$this->registerJsFile($this->theme->baseUrl . "/assets/js/select2.min.js");
