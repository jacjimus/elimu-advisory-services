<?php

namespace app\modules\admin\controllers;


use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\models\Search;
use \app\modules\admin\models\ProgrammeTypes;
use \app\modules\admin\models\UserResult;
use app\modules\admin\models\UserSearchParams;
use yii\helpers\Json;
use app\modules\admin\models\EducationSystem;
/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    
    /**
     * @param string           $id
     * @param \yii\base\Module $module
     * @param Finder           $finder
     * @param array            $config
     */
    public function __construct($id, $module, $config = [])
    {
        $this->id = $id;
        $this->module = $module;
        $this->layout = "@app/themes/eac/layouts/dashboard";
        parent::__construct($id, $module, $config);
    }
    /**
     * Renders the index view for the module
     * @return string
     */
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['search'],
                'rules' => [
                    [
                        'actions' => ['search' , 'search2' , 'error'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actionSearch()
    {
        $step = 1;
        $search = new Search;
        if ($search->load(Yii::$app->request->post())) {
            
            $search->date_of_search = date('Y-m-d');
            $search->user_id = Yii::$app->user->id;
            if($search->save()){
                 $usersearchparam = new UserSearchParams;
                 $usersearchparam->param_id = 16;
                 $usersearchparam->search_id = $search->search_id;
                 $usersearchparam->param_value = Yii::$app->request->post()['Search']['education_system'];
                if($usersearchparam->save())
                    return $this->redirect(['search2' , 'id' => $search->search_id]);
             }
            
       } 
       return $this->render('search', [
            'user_search_model' => $search,
            'step' => $step
        ]);
   
    }
    
    public function actionSearch2($id)
    {
        $step = 2;
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            
       } 
       return $this->render('search2', [
            'user_search_model' => $model,
            'step' => $step
        ]);
   
    }
    
    
    public function actionError()
    {
        return $this->render('error');
    }
    
    public function actionSubcat() {
    $out = [];
    
   if (isset($_POST['depdrop_parents'])) {
        $parents = $_POST['depdrop_parents'];
        if ($parents != null) {
            $cat_id = $parents[0];
            $param1 = null;
            $param2 = null;
            if (!empty($_POST['depdrop_params'])) {
                $params = $_POST['depdrop_params'];
                $param1 = $params[0]; // get the value of input-type-1
                $param2 = $params[1]; // get the value of input-type-2
            }
 
            $out = self::getSubCatList1($cat_id, $param1, $param2); 
                 
            
            //$selected = self::getDefaultSubCat($cat_id);
            // the getDefaultSubCat function will query the database
            // and return the default sub cat for the cat_id
            
            echo Json::encode(['output'=>$out, 'selected'=> '']);
            return;
        }
    }
    echo Json::encode(['output'=>'', 'selected'=>'']);
}

protected static function getSubCatList1($id , $param1 , $param2)
{
    $data = [];
   $educs = \app\modules\admin\models\EducationSystem::find()->all();
   foreach ($educs As $item):
       $data[] = ['id'=>$item->system_id, 'name'=>$item->system_name]; 
   endforeach;
    return $data;
}

 protected function findModel($id)
    {
        if (($model = Search::findOne(['search_id'=> $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
