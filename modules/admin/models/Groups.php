<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "groups".
 *
 * @property integer $group_no
 * @property string $group_desc
 * @property string $group_name
 * @property integer $max_subject
 * @property integer $min_subject
 *
 * @property Requirements[] $requirements
 * @property SubjectGroup[] $subjectGroups
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_no'], 'required'],
            [['group_no', 'max_subject', 'min_subject'], 'integer'],
            [['group_desc', 'group_name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_no' => Yii::t('app', 'Group No'),
            'group_desc' => Yii::t('app', 'Group Desc'),
            'group_name' => Yii::t('app', 'Group Name'),
            'max_subject' => Yii::t('app', 'Max Subject'),
            'min_subject' => Yii::t('app', 'Min Subject'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequirements()
    {
        return $this->hasMany(Requirements::className(), ['group_no' => 'group_no']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectGroups()
    {
        return $this->hasMany(SubjectGroup::className(), ['group_no' => 'group_no']);
    }

    /**
     * @inheritdoc
     * @return GroupsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GroupsQuery(get_called_class());
    }
}
