<?php

namespace app\modules\admin\models;

/**
 * This is the ActiveQuery class for [[UserResult]].
 *
 * @see UserResult
 */
class UserResultQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UserResult[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserResult|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
