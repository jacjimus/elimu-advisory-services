<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\admin\models\SubjectGroup;
/**
 * This is the model class for table "subject".
 *
 * @property string $subject_code
 * @property string $subject_abbr
 * @property string $subject_name
 *
 * @property SubjectGroup[] $subjectGroups
 * @property UserClusterSubjects[] $userClusterSubjects
 * @property UserResult[] $userResults
 */
class Subject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_code'], 'required'],
            [['subject_code'], 'string', 'max' => 10],
            [['subject_abbr'], 'string', 'max' => 5],
            [['subject_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subject_code' => 'Subject Code',
            'subject_abbr' => 'Subject Abbr',
            'subject_name' => 'Subject Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectGroups()
    {
        return $this->hasMany(SubjectGroup::className(), ['subject_code' => 'subject_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserClusterSubjects()
    {
        //return $this->hasMany(UserClusterSubjects::className(), ['subject_code' => 'subject_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserResults()
    {
        return $this->hasMany(UserResult::className(), ['subject_code' => 'subject_code']);
    }

    /**
     * @inheritdoc
     * @return SubjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubjectQuery(get_called_class());
    }
}
