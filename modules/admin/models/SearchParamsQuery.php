<?php

namespace app\modules\admin\models;

/**
 * This is the ActiveQuery class for [[SearchParam]].
 *
 * @see SearchParam
 */
class SearchParamsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return SearchParam[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SearchParam|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
