<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "subject_group".
 *
 * @property integer $subject_group_id
 * @property integer $group_no
 * @property string $subject_code
 * @property integer $optionA
 * @property integer $optionB
 *
 * @property Subject $subjectCode
 * @property Groups $groupNo
 */
class SubjectGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_no', 'optionA', 'optionB'], 'integer'],
            [['subject_code'], 'string', 'max' => 10],
            [['subject_code'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_code' => 'subject_code']],
            [['group_no'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['group_no' => 'group_no']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subject_group_id' => Yii::t('app', 'Subject Group ID'),
            'group_no' => Yii::t('app', 'Group No'),
            'subject_code' => Yii::t('app', 'Subject Code'),
            'optionA' => Yii::t('app', 'Option A'),
            'optionB' => Yii::t('app', 'Option B'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectCode()
    {
        return $this->hasOne(Subject::className(), ['subject_code' => 'subject_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupNo()
    {
        return $this->hasOne(Groups::className(), ['group_no' => 'group_no']);
    }

    /**
     * @inheritdoc
     * @return SubjectGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubjectGroupQuery(get_called_class());
    }
}
