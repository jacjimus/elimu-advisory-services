<?php

namespace app\modules\admin\models;

/**
 * This is the ActiveQuery class for [[Grades]].
 *
 * @see Grades
 */
class GradesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Grades[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Grades|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
