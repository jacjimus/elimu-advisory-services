<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\admin\models\SearchParams;
use app\models\Search;
/**
 * This is the model class for table "user_search_params".
 *
 * @property integer $user_search_id
 * @property integer $param_id
 * @property integer $search_id
 * @property string $param_value
 *
 * @property SearchParam $param
 * @property Search $search
 */
class UserSearchParams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_search_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['param_id', 'search_id'], 'integer'],
            [['param_value'], 'string', 'max' => 250],
            [['param_id'], 'exist', 'skipOnError' => true, 'targetClass' => SearchParams::className(), 'targetAttribute' => ['param_id' => 'param_id']],
            [['search_id'], 'exist', 'skipOnError' => true, 'targetClass' => Search::className(), 'targetAttribute' => ['search_id' => 'search_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_search_id' => 'User Search ID',
            'param_id' => 'Param ID',
            'search_id' => 'Search ID',
            'param_value' => 'Param Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParam()
    {
        return $this->hasOne(SearchParam::className(), ['param_id' => 'param_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearch()
    {
        return $this->hasOne(Search::className(), ['search_id' => 'search_id']);
    }

    /**
     * @inheritdoc
     * @return UserSearchParamsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserSearchParamsQuery(get_called_class());
    }
}
