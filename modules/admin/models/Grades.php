<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "grades".
 *
 * @property integer $grade_id
 * @property string $grade
 * @property integer $grade_points
 */
class Grades extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grades';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grade_points'], 'integer'],
            [['grade'], 'string', 'max' => 2],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'grade_id' => Yii::t('app', 'Grade ID'),
            'grade' => Yii::t('app', 'Grade'),
            'grade_points' => Yii::t('app', 'Grade Points'),
        ];
    }

    /**
     * @inheritdoc
     * @return GradesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GradesQuery(get_called_class());
    }
}
