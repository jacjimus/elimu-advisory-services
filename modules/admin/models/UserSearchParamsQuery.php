<?php

namespace app\modules\admin\models;

/**
 * This is the ActiveQuery class for [[UserSearchParams]].
 *
 * @see UserSearchParams
 */
class UserSearchParamsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return UserSearchParams[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return UserSearchParams|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
