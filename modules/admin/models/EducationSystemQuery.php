<?php

namespace app\modules\admin\models;

/**
 * This is the ActiveQuery class for [[EducationSystem]].
 *
 * @see EducationSystem
 */
class EducationSystemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return EducationSystem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EducationSystem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
