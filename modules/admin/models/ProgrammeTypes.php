<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "programme_types".
 *
 * @property integer $type_id
 * @property string $type_name
 *
 * @property Programme[] $programmes
 */
class ProgrammeTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'programme_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_name'], 'required'],
            [['type_name'], 'string', 'max' => 200],
            [['type_name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_id' => 'ID',
            'type_name' => 'Programme Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgrammes()
    {
        return $this->hasMany(Programme::className(), ['type_id' => 'type_id']);
    }

    /**
     * @inheritdoc
     * @return ProgrammeTypesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProgrammeTypesQuery(get_called_class());
    }
}
