<?php

namespace app\modules\admin\models;
use dektrium\user\models\User;
use Yii;

/**
 * This is the model class for table "user_result".
 *
 * @property integer $user_result_id
 * @property integer $user_id
 * @property string $search_id
 * @property string $subject_code
 * @property integer $subject_grade
 * @property double $subject_pi
 * @property string $bap_flag
 * @property double $ratio
 *
 * @property Subject $subjectCode
 * @property Users $user
 */
class UserResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_result';
    }

    
    
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'search_id', 'subject_grade', 'bap_flag'], 'integer'],
            [['subject_pi', 'ratio'], 'number'],
            [['subject_code'], 'string', 'max' => 10],
            [['user_id', 'search_id', 'subject_code'], 'unique', 'targetAttribute' => ['user_id', 'search_id', 'subject_code'], 'message' => 'The combination of User ID, Search ID and Subject Code has already been taken.'],
            [['subject_code'], 'exist', 'skipOnError' => true, 'targetClass' => Subject::className(), 'targetAttribute' => ['subject_code' => 'subject_code']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_result_id' => 'User Result ID',
            'user_id' => 'User ID',
            'search_id' => 'Search ID',
            'subject_code' => 'Subject Code',
            'subject_grade' => 'Subject Grade',
            'subject_pi' => 'Subject Pi',
            'bap_flag' => 'Bap Flag',
            'ratio' => 'Ratio',
           
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectCode()
    {
        return $this->hasOne(Subject::className(), ['subject_code' => 'subject_code']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return UserResultQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserResultQuery(get_called_class());
    }
}
