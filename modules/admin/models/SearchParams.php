<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "search_param".
 *
 * @property integer $param_id
 * @property string $param_name
 * @property integer $param_code
 * @property string $search_level
 * @property string $source_table
 * @property string $source_column
 * @property integer $param_used_in_search
 *
 * @property UserSearchParams[] $userSearchParams
 */
class SearchParams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search_param';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['param_name', 'param_code', 'search_level', 'source_table', 'source_column'], 'required'],
            [['param_code', 'param_used_in_search'], 'integer'],
            [['param_name'], 'string', 'max' => 50],
            [['search_level', 'source_table', 'source_column'], 'string', 'max' => 80],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'param_id' => 'Param ID',
            'param_name' => 'Param Name',
            'param_code' => 'Param Code',
            'search_level' => 'Search Level',
            'source_table' => 'Source Table',
            'source_column' => 'Source Column',
            'param_used_in_search' => 'Param Used In Search',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSearchParams()
    {
        return $this->hasMany(UserSearchParams::className(), ['param_id' => 'param_id']);
    }

    /**
     * @inheritdoc
     * @return UserSearchParamsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserSearchParamsQuery(get_called_class());
    }
}
