<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "education_system".
 *
 * @property integer $system_id
 * @property string $system_name
 * @property integer $duration
 * @property string $full_name
 * @property integer $examination_body_id
 * @property string $education_category
 *
 * @property ExaminingBody $examinationBody
 * @property LevelQualifications[] $levelQualifications
 */
class EducationSystem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'education_system';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['system_name', 'duration', 'full_name', 'examination_body_id', 'education_category'], 'required'],
            [['duration', 'examination_body_id'], 'integer'],
            [['system_name', 'education_category'], 'string', 'max' => 100],
            [['full_name'], 'string', 'max' => 200],
            [['examination_body_id'], 'exist', 'skipOnError' => true, 'targetClass' => ExaminingBody::className(), 'targetAttribute' => ['examination_body_id' => 'examining_body_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'system_id' => 'System ID',
            'system_name' => 'System Name',
            'duration' => 'Duration',
            'full_name' => 'Full Name',
            'examination_body_id' => 'Examination Body ID',
            'education_category' => 'Education Category',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExaminationBody()
    {
        return $this->hasOne(ExaminingBody::className(), ['examining_body_id' => 'examination_body_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevelQualifications()
    {
        return $this->hasMany(LevelQualifications::className(), ['system_id' => 'system_id']);
    }

    /**
     * @inheritdoc
     * @return EducationSystemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EducationSystemQuery(get_called_class());
    }
}
