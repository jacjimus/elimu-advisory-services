<?php

namespace app\models;

use Yii;
use dektrium\user\models\User;


/**
 * This is the model class for table "search".
 *
 * @property integer $search_id
 * @property integer $user_id
 * @property integer $exam_year
 * @property string $date_of_search
 * @property string $status_of_exam_used
 *
 * @property Users $user
 * @property SearchResult[] $searchResults
 * @property UserSearchParams[] $userSearchParams
 */
class Search extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    
    /**
     * Integer education_cat_id
     * @education_cat_id
     */
    
    public $education_cat_id;
    /**
     * Integer education_system
     * @education_system
     */
    public $education_system;
    
    public static function tableName()
    {
        return 'search';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['education_system', 'education_cat_id' ], 'required' , 'message' => 'please select {attribute}'],
            [['search_id', 'user_id'], 'integer'],
            [['date_of_search'], 'safe'],
            [['status_of_exam_used'], 'string', 'max' => 60],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'search_id' => 'Search ID',
            'user_id' => 'User ID',
            'date_of_search' => 'Date Of Search',
            'status_of_exam_used' => 'Status Of Exam Used',
            'education_system' => 'Highest Qualification',
            'education_cat_id' => 'Search category',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchResults()
    {
        return $this->hasMany(SearchResult::className(), ['search_id' => 'search_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserSearchParams()
    {
        return $this->hasMany(UserSearchParams::className(), ['search_id' => 'search_id']);
    }

    /**
     * @inheritdoc
     * @return DisciplineQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DisciplineQuery(get_called_class());
    }
}
