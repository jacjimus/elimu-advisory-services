<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;
use dektrium\user\models\User As UserForm;
/**
 * Description of Users
 *
 * @author James Makau
 */
class User extends UserForm{
   /**
     * Add  new fields
     * @var string
     */
    public $names;
    public $gender;
    
    public $year_of_birth;
    
    public $mobile_phone;
    
    
     public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['names', 'string', 'max' => 50];
        return $rules;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['names'] = \Yii::t('user', 'Name');
        $labels['gender'] = \Yii::t('user', 'Gender');
        $labels['year_of_birth'] = \Yii::t('user', 'Year of Birth (yyyy)');
        $labels['mobile_phone'] = \Yii::t('user', 'Mobile Number');
        return $labels;
    }

}
