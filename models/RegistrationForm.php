<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;
use dektrium\user\models\RegistrationForm As RegForm;
/**
 * Description of RegistrationForm
 *
 * @author James Makau
 */
class RegistrationForm extends RegForm{
    
        
    
    
    public function rules()
    {
        $rules = parent::rules();
        $rules['usernameLength'] = ['username', 'string', 'min' => 5, 'max' => 25];
        $rules[] = ['names', 'string', 'max' => 50];
        return $rules;
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['names'] = \Yii::t('user', 'Name');
        $labels['gender'] = \Yii::t('user', 'Gender');
        $labels['year_of_birth'] = \Yii::t('user', 'Year of Birth (yyyy)');
        $labels['mobile_number'] = \Yii::t('user', 'Mobile Number');
        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function loadAttributes(User $user)
    {
        // here is the magic happens
        $user->setAttributes([
            'email'    => $this->email,
            'username' => $this->username,
            'password' => $this->password,
        ]);
        
    }
    
    
}
